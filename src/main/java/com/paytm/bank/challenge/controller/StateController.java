package com.paytm.bank.challenge.controller;

import com.paytm.bank.challenge.constants.enums.FlowType;
import com.paytm.bank.challenge.model.UpdateStatusRequest;
import com.paytm.bank.challenge.model.UpdateStatusResponse;
import com.paytm.bank.challenge.service.StateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class StateController {

  private final StateService stateService;

  @Autowired
  public StateController(StateService stateService) {
    this.stateService = stateService;
  }

  @PostMapping("/{flowType}/update")
  public UpdateStatusResponse update(
      @PathVariable("flowType") FlowType flowType, UpdateStatusRequest request) {

    return stateService.updateStatus(flowType, request);
  }
}
