package com.paytm.bank.challenge.data.entity;

import com.paytm.bank.challenge.challenges.ChallengeType;
import com.paytm.bank.challenge.statemachine.Event;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Data
@Entity
@Table(name = "trail_log")
public class TrailLog {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "session_id")
  @NotNull
  private String sessionId;

  @Enumerated(EnumType.STRING)
  @Column(name = "challenge")
  @NotNull
  private ChallengeType challenge;

  @Enumerated(EnumType.STRING)
  @Column(name = "event")
  @NotNull
  private Event event;

  @Column(name = "createdAt", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
  @CreationTimestamp
  protected Date createdAt;

  @Column(name = "updatedAt", insertable = false, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
  @UpdateTimestamp
  protected Date updatedAt;

}
