package com.paytm.bank.challenge.data.repository;

import com.paytm.bank.challenge.data.entity.TrailLog;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface TrailLogRepository extends CrudRepository<TrailLog, Long> {

  List<TrailLog> findBySessionIdOrderByCreatedAtAsc(String sessionId);
}
