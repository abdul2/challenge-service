package com.paytm.bank.challenge.service.impl;

import com.paytm.bank.challenge.challenges.Challenge;
import com.paytm.bank.challenge.challenges.ChallengeType;
import com.paytm.bank.challenge.constants.enums.FlowType;
import com.paytm.bank.challenge.data.entity.TrailLog;
import com.paytm.bank.challenge.data.repository.TrailLogRepository;
import com.paytm.bank.challenge.model.UpdateStatusRequest;
import com.paytm.bank.challenge.model.UpdateStatusResponse;
import com.paytm.bank.challenge.service.StateService;
import com.paytm.bank.challenge.statemachine.Event;
import com.paytm.bank.challenge.statemachine.StateMachine;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StateServiceImpl implements StateService {

  private final TrailLogRepository trailLogRepository;

  @Autowired
  public StateServiceImpl(TrailLogRepository trailLogRepository) {
    this.trailLogRepository = trailLogRepository;
  }

  @Override
  public UpdateStatusResponse updateStatus(FlowType flowType, UpdateStatusRequest request) {

    // add current request to traillog table.
    // get statemachine by flow type,
    // replay events checking consistency with current statemachine
    //
    StateMachine stateMachine = flowType.getStateMachine();
    List<TrailLog> trailLogs = trailLogRepository
        .findBySessionIdOrderByCreatedAtAsc(request.getSessionId());

    List<Event> events = trailLogs.stream().map(TrailLog::getEvent).collect(Collectors.toList());

    Challenge current = stateMachine.replayEvents(events);
    Challenge next = stateMachine.next(current, request.getStatus().getEvent());

    if (current.isFinal() || next.isFinal()) {
      return new UpdateStatusResponse(buildJwt(), ChallengeType.COMPLETED);
    }
    return new UpdateStatusResponse(buildJwt(), next.getName());
  }

  private String buildJwt() {
    return null;
  }
}
