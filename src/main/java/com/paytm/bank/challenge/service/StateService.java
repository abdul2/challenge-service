package com.paytm.bank.challenge.service;

import com.paytm.bank.challenge.constants.enums.FlowType;
import com.paytm.bank.challenge.model.UpdateStatusRequest;
import com.paytm.bank.challenge.model.UpdateStatusResponse;

public interface StateService {

  UpdateStatusResponse updateStatus(FlowType flowType, UpdateStatusRequest request);

}
