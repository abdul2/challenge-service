package com.paytm.bank.challenge.statemachine;

import com.paytm.bank.challenge.challenges.Challenge;
import com.paytm.bank.challenge.challenges.InitChallenge;
import com.paytm.bank.challenge.challenges.OtpChallenge;
import com.paytm.bank.challenge.challenges.PaytmLogin;
import com.paytm.bank.challenge.constants.StateMachineName;
import java.util.Arrays;
import java.util.List;
import org.springframework.stereotype.Component;

@Component(StateMachineName.EXAMPLE)
public class ExampleStateMachine extends StateMachine {

  @Override
  public List<Transition> transitions() {

    Challenge otpChallenge = new OtpChallenge(0, 3, false);

    return Arrays.asList(
        new Transition(new InitChallenge(), Event.SUCCESS, c -> otpChallenge),
        new Transition(otpChallenge, Event.SUCCESS, c -> new PaytmLogin(0, 1, true))
//        Transition.success(otpChallenge, c -> new PaytmLogin(1, 3)),
//        Transition.failure(otpChallenge)
    );
  }
}
