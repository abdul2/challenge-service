package com.paytm.bank.challenge.statemachine;

import static org.springframework.util.CollectionUtils.isEmpty;

import com.paytm.bank.challenge.challenges.Challenge;
import com.paytm.bank.challenge.challenges.InitChallenge;
import com.paytm.bank.challenge.exception.StateMachineException;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class StateMachine {


  public abstract List<Transition> transitions();

  StateMachine() {
    StateMachineUtils.verifyStateMachine(this);
  }

  public final Challenge next(final Challenge current, Event event) {

    if (current.isFinal()) {
      return current;
    }

    List<Transition> transitions = transitions().stream()
        .filter(transitionPredicate(current, event))
        .collect(Collectors.toList());

    if (transitions.isEmpty()) {
      throw new StateMachineException(
          "State Machine cannot be in this state. Current " + current.getName() + " Event: "
              + event);
    }

    if (transitions.size() > 1) {
      throw new StateMachineException("State Machine cannot have more than 1 output State. "
          + "Current " + current.getName() + " Event: " + event);
    }

    return transitions.get(0).to.apply(current);
  }

  private Predicate<Transition> transitionPredicate(Challenge current, Event event) {
    return t -> t.from.getName() == current.getName()
        && t.from.getId() == current.getId()
        && t.event == event;
  }

  public final Challenge replayEvents(List<Event> events) {

    if (isEmpty(events)) {
      return new InitChallenge();
    }

    Challenge challenge = new InitChallenge();

    for (Event event : events) {
      challenge = next(challenge, event);
      // constraint handling.
    }
    return challenge;
  }

}
