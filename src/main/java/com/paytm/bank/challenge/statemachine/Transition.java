package com.paytm.bank.challenge.statemachine;

import com.paytm.bank.challenge.challenges.Challenge;
import java.util.function.Function;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
class Transition {

  Challenge                      from;
  Event                          event;
  Function<Challenge, Challenge> to;

  static Transition success(Challenge from, Function<Challenge, Challenge> to) {
    Transition transition = new Transition();

    transition.setFrom(from);
    transition.setEvent(Event.SUCCESS);
    transition.setTo(to);
    return transition;
  }

  static Transition failure(Challenge from, Function<Challenge, Challenge> to) {
    Transition transition = new Transition();

    transition.setFrom(from);
    transition.setEvent(Event.FAILURE);
    transition.setTo(to);
    return transition;
  }

  static Transition failure(Challenge from) {
    Transition transition = new Transition();

    transition.setFrom(from);
    transition.setEvent(Event.FAILURE);
    transition.setTo(Function.identity());
    return transition;
  }
}

