package com.paytm.bank.challenge.statemachine;

public enum Event {
  SUCCESS,
  FAILURE;
}