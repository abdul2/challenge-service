package com.paytm.bank.challenge.challenges;

public enum ChallengeType {

  INIT,
  OTP,
  PASSCODE,
  PAYTM_LOGIN,
  COMPLETED;
}
