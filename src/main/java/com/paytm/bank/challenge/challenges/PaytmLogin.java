package com.paytm.bank.challenge.challenges;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PaytmLogin implements Challenge {

  public static final ChallengeType name = ChallengeType.PAYTM_LOGIN;

  private int     id;
  private int     maxRetries;
  private boolean finalState;

  @Override
  public int getMaxRetries() {
    return maxRetries;
  }

  @Override
  public ChallengeType getName() {
    return name;
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public boolean isFinal() {
    return finalState;
  }
}
