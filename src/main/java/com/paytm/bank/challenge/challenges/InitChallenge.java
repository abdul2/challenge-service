package com.paytm.bank.challenge.challenges;

public class InitChallenge implements Challenge {

  @Override
  public int getMaxRetries() {
    return 0;
  }

  @Override
  public ChallengeType getName() {
    return ChallengeType.INIT;
  }

  @Override
  public int getId() {
    return 0;
  }

  @Override
  public boolean isFinal() {
    return false;
  }
}
