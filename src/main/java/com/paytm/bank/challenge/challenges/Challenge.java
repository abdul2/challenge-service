package com.paytm.bank.challenge.challenges;

public interface Challenge {

  int getMaxRetries();

  ChallengeType getName();

  int getId();

  boolean isFinal();

}
