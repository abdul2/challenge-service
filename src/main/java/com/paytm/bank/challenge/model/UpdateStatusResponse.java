package com.paytm.bank.challenge.model;

import com.paytm.bank.challenge.challenges.ChallengeType;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UpdateStatusResponse {

  private String jwtToken;

  private ChallengeType nextChallenge;
}
