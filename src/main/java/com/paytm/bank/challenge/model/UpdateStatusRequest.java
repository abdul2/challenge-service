package com.paytm.bank.challenge.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UpdateStatusRequest {

  private OperationStatus status;

  private String sessionId;

}
