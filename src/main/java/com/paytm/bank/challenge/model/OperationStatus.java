package com.paytm.bank.challenge.model;

import com.paytm.bank.challenge.statemachine.Event;
import lombok.Getter;

@Getter
public enum OperationStatus {

  SUCCESS(Event.SUCCESS),
  FAILURE(Event.FAILURE);

  private Event event;

  OperationStatus(Event event) {
    this.event = event;
  }
}
