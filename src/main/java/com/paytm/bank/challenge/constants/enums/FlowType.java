package com.paytm.bank.challenge.constants.enums;

import com.paytm.bank.challenge.constants.StateMachineName;
import com.paytm.bank.challenge.statemachine.StateMachine;
import java.util.Map;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Getter
@Slf4j
public enum FlowType {

  EXAMPLE(StateMachineName.EXAMPLE);


  @Setter
  private String stateMachineName;

  @Setter
  private StateMachine stateMachine;

  FlowType(String stateMachineName) {
    this.stateMachineName = stateMachineName;
  }

  @Component
  private static class FlowTypeInner {

    @Autowired
    private Map<String, StateMachine> stateMachineMap;

    @PostConstruct
    private void init() {
      for (FlowType flowType : values()) {
        StateMachine stateMachine = stateMachineMap.get(flowType.getStateMachineName());

        if (stateMachine == null) {
          log.error("No State Machine found for Flow Type: {}", flowType);
          throw new RuntimeException("No State Machine found for Flow Type: " + flowType);
        } else {
          flowType.setStateMachine(stateMachine);
        }
      }
    }

  }
}
